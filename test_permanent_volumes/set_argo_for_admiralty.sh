kubectl create ns argo

kubectl apply -n argo -f https://raw.githubusercontent.com/argoproj/argo-workflows/stable/manifests/install.yaml


########################
#
#  Launch workflow
#
########################

# First example
#argo --context $ARGO_CLUSTER submit --serviceaccount argo-workflow --watch https://raw.githubusercontent.com/admiraltyio/admiralty/master/examples/argo-workflows/blog-scenario-a-singlecluster.yaml
#

# Second example
#argo --context $ARGO_CLUSTER submit --serviceaccount argo-workflow --watch https://raw.githubusercontent.com/admiraltyio/admiralty/master/examples/argo-workflows/blog-scenario-a-multicluster.yaml

# Third example
#curl -X GET "https://raw.githubusercontent.com/admiraltyio/admiralty/master/examples/argo-workflows/blog-scenario-b.yaml" | sed "s|cluster2|$NON_ARGO_CLUSTER|g" > blog-scenario-b.yaml
#argo --context $ARGO_CLUSTER submit --serviceaccount argo-workflow --watch blog-scenario-b.yaml 

