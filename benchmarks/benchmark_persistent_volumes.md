# Benchmark Persistent Volumes

## Test on `PersistentVolumeClaim`
### Setup
Two clusters running on Google Kubernetes Engine. One source cluster (cluster-cd) and one target cluster (cluster-1).
I have created 2PersistentVolumeClaims on each cluster:
| Cluster | PVC |
--- | ---
|cluster-cd | pvc-cd,  pvc-demo
|cluster-1  | pvc-1, pvc-demo

Note that the pvc-demo do not point to the same PersistantVolume. Just the name is the same.
Also, I use the following jobs to test them (extracted from the quick start guide)

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: admiralty-pvc-test
spec:
  template:
    metadata:
      annotations:
        multicluster.admiralty.io/elect: ""
    spec:
      volumes:
        - name: task-pv-storage
          persistentVolumeClaim:
            claimName: pvc-demo
      containers:
      - name: c
        image: busybox
        command: ["sh", "-c", "echo hello world: && echo hello world >> /mnt/data/hello.txt && ifconfig && df -h"]
        # command: ["sh", "-c", "cat /mnt/data/hello.txt && ifconfig && echo ------- && df -h"]
        volumeMounts:
          - mountPath: "/mnt/data/"
            name: task-pv-storage
        resources:
          requests:
            cpu: 100m
      restartPolicy: Never
```

### Case 1

I set claimName: pvc-cd (pvc on the source cluster).
The pods stay in Pending status (in source and target clusters) and the pod description in the source cluster context gives me the following error.

Warning  FailedScheduling  44s (x3 over 111s)  admiralty-proxy  0/4 nodes are available: 1 , 3 node(s) didn't match node selector.

### Case 2

I set claimName: pvc-1 (pvc on the target cluster).
The pods stay in Pending status (only in the source cluster this time, pods does not even show up in target cluster).
The pod description in the source cluster context gives me the following error.

Warning  FailedScheduling  48s (x3 over 118s)  admiralty-proxy  persistentvolumeclaim "pvc-1" not found

### Case 3

I set claimName: pvc-demo (pvc that exists on both cluster, but refers to different locations)
In this case, it seems to be working. However, the command echo hello world >> /mnt/data/hello.txtis written
on the pvc of the target clusters.

### Conclusion
It seems that the `PersistentVolume` cannot be propagated through multicluster-scheduler unless is exists on both clusters. 
In that case, the system effectively mounted is that of the target cluster.
This situation is not that we want for some multicluster workflows, since all cluster won't be able to access the same `PersistentVolume`.
