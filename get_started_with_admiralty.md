# Multicluster-scheduler 

This is a small guide on how to get started with Multicluster-scheduler.
For more information, check the official Admiralty [Quick Start Guide](https://admiralty.io/docs/quick_start/).

You can find all the scripts needed to set Admiralty in the [scripts](./scripts) folder.
In order to get started, you need to have at least two kubernetes clusters and `kubectl`installed.

You will have to the clusters name in the `config.sh` script. 
```yaml
SOURCE_CLUSTER="MY_SOURCE_CLUSTER"
TARGET_CLUSTER="MY_TARGET_CLUSTER"
```

That script will be sourced by the other scripts to retrieve the name of the clusters to use.

We first consider that the Admiralty workloads will be launched in the default namespace, so be sure to have the following lines set in `config.sh`
```
NAMESPACE_TARGET=default
NAMESPACE_SOURCE=default
```

For more information on how to use non-default namespaces, see [Admiralty in non-default namespace](#admiralty-in-non-default-namespace) section.

## Admiralty installation and set-up
The first step is to execute the [`set_multicluster-scheduler.sh`](./scripts/set_multicluster-scheduler.sh) script.
That script will source the `config.sh` file and execute several functions in order to install and set-up Admiralty.
The functions to be executed are:
* `admiralty_installation` will install Admiralty and all the dependencies on the clusters.
* `set_regions` will label each cluster with a different region. (outdated, but not required!)
* `create_secret` will create a service account on the target cluster and extract the token corresponding to that service account. It will then create a secret in the source cluster to connect to that service account.
* `set_clusters` will set the relationship between each clusters (source or target) and enable multicluster-scheduler at the namespace level on the source cluster.

Once the clusters and namesaces are properly set in the `config.sh` file, you can install Admiralty
```bash
sh set_multicluster-scheduler.sh
```

Once the installation is done, you should see, on the source cluster, a virtual node representing the target cluster
```bash
kubectl --context $SOURCE_CLUSTER get nodes

NAME                                                                         STATUS   ROLES     AGE     VERSION
admiralty-default-secret-gke-irt-sb-us-central1-c-cluster-sbo-2-bb16e78da8   Ready    cluster   27s
gke-cluster-sbo-manageme-default-pool-757d4dec-66sp                          Ready    <none>    4m39s   v1.18.17-gke.100
gke-cluster-sbo-manageme-default-pool-757d4dec-qp5t                          Ready    <none>    4m42s   v1.18.17-gke.100
gke-cluster-sbo-manageme-default-pool-757d4dec-wtd3                          Ready    <none>    4m42s   v1.18.17-gke.100
```
The virtual node is represented by the string `admiralty` + $NAMESPACE_SOURCE + $TARGET_CLUSTER + random_string.
In this case `admiralty-default-secret-gke-irt-sb-us-central1-c-cluster-sbo-2-bb16e78da8`.

## Running jobs
You can now create jobs with the `create_admiralty_jobs.sh`
```bash
sh create_admiralty_jobs.sh
```
That script will create a serie of jobs on the source cluster. These jobs are annotated with `multicluster.admiralty.io/elect:""` so they will be deployed on the target clusters.

You can run the `check_pods.sh` script to the check the status of the pods, both on the source and the target cluster. That script will source 
the `config.sh` file to show you the corresponding information.

## Clean the clusters
Once you are done, you can clean everything with the `clean.sh` script.
That script will run over all the namespace and delete most of the stuff you have installed.
There is still some work to do here in oder to delete everything (clusterrole, clusterrolebinding, etc.).

```bash
sh clean.sh
```

Note that when some pods are stuck, that script does not work. 


## Admiralty in non-default namespace
Some additional tools, such as kubecost, may require the pods to run in a non-default
namespace. You can configure Admiralty to do so. 

There are 2 different ways to do that. In the first approach, we consider only the namespace we want to use. 
This means that the `serviceaccount` and `secret` used for the authentication of the clusters are placed in the
same `namespace` as the execution of the pods.
In the second approach, the `serviceaccount` on the target cluster is stored in a different `namespace` to that
used to execute the pods. This requires the use of `ClusterRole` and `ClusterRoleBinding`.

### Basic approach
In the first approach, the set-up is similaire to a basic Admiralty installation. 
The only difference lies in the namespaces you set in the `config.sh`. Be sure to put the same namespace
for both source and target clutser, for example
```bash
NAMESPACE_TARGET=kubecost-ns
NAMESPACE_SOURCE=kubecost-ns
```
The `set_multicluster-scheduler.sh` will create the corresponding namespaces and create the
`serviceaccount` and `secret` in those namespaces. Furthermore, the `Target` and `Source` objects, required for scheduling,  will
also be created in those namespaces. 

You can then install Admiralty
```bash
sh set_multicluster-scheduler.sh
```
Launch the jobs
```bash
sh create_admiralty_jobs.sh
```
Note that the jobs will be launched in the `kubecost-ns` namespace on both source and target clusters.

Check the pods status
```bash
sh check_pods.sh
```

All the scripts mentionned above source the `config.sh` script, so they know which namespace
to use/check.


### RBAC-based approach 
In this approach, the target cluster `serviceaccount` created for the cluster authentication can be placed in any `namespace`. 
We then use Right Based Access Control (RBAC) to be able to launch an Admiralty pods in any `namespace` on the target clusters.

For this, the `set_multicluster-scheduler.sh` will check if the namespaces in the source and target clusters are the same.
If not, it will run the [`admiralty-rbac.sh`](./scripts/admiralty-rbac.sh) script that will create a dedicated `ClusterRole` and `ClusterRoleBinding` 
on the targets clusters. 
In addition, `ClusterSource` object will be create on the target cluster, intead of a `Source` object.
For more information, you can read the [Admiralty doc on scheduling](https://admiralty.io/docs/operator_guide/scheduling)

Set the namespaces
```bash
NAMESPACE_TARGET=kubecost-ns
NAMESPACE_SOURCE=default
```

Update the namespace and serviceaccount in the `admiralty-rbac.sh`

and install admiralty
```bash
sh set_multicluster-scheduler.sh
```
Launch the jobs
```bash
sh create_admiralty_jobs.sh
```
and check the pods status
```bash
sh check_pods.sh
```
Note that the pods will be running in the `default` namespace on both cluster, since that is the namespace specified in 
the source cluster (`NAMESPACE_SOURCE=default`)


## Troubleshooting
Here is a small list of problems encountered during the test phase.

## Wrong Kubernetes API server ports
By default, the Kubernetes API server listens on port 6443, as said in the 
[official documentation](https://kubernetes.io/docs/concepts/security/controlling-access/#api-server-ports-and-ips).
However, some cloud providers might decide to change that port. That is the case with GCP and GKE. If you use GKE, you need to 
set the API server port to 443. 

## Narrow down the issue
If you detect wrong behavior, here are a few steps you can check in order to narrow down the problem.
### Describe the nodes
The source cluster will create virtual nodes that represents the target cluster. You can get a list of the virtual nodes by running, 
in the context of the source cluster, the `kubectl get nodes` command. The output should looks like this:
```bash
admiralty-default-secret-gke-irt-sb-us-central1-c-cluster-sbo-2-bb16e78da8   Ready   cluster   57m
gke-cluster-sbo-manageme-default-pool-4c82dfbe-72sh                          Ready      <none>    66m   v1.18.17-gke.100
gke-cluster-sbo-manageme-default-pool-4c82dfbe-9f36                          Ready      <none>    66m   v1.18.17-gke.100
gke-cluster-sbo-manageme-default-pool-4c82dfbe-jxkw                          Ready      <none>    66m   v1.18.17-gke.100
```

We can see the 3 nodes of the cluster and the Admiralty virtual node representing a target cluster. You can describe the node and see what is inside
```bash
kubectl describe nodes admiralty-default-secret-gke-irt-sb-us-central1-c-cluster-sbo-2-bb16e78da8
```
The insteresting part of the output is the last part, where we can see the `Allocated resources`:
```bash
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource                   Requests    Limits
  --------                   --------    ------
  cpu                        103m (3%)   0 (0%)
  memory                     250Mi (6%)  550Mi (13%)
  ephemeral-storage          0 (0%)      0 (0%)
  hugepages-2Mi              0 (0%)      0 (0%)
  attachable-volumes-gce-pd  0           0
Events:                      <none>
```
Here we can that the cpu and memory request are different than 0. That is what you expect. If the requested cpu and memory are at 0, there is a problem somewhere.

### Check Admiralty logs
You can check the Admiralty logs. In the namespace admiralty, you should see a pods named `pod/admiralty-multicluster-scheduler-controller-manager-XXXXXXXXX`
```bash
pod/admiralty-multicluster-scheduler-controller-manager-86bfcbw9n7n   1/1     Running   0          8m30s
```
You can check the log of that pod. A healthy log should look like this
```bash
kubectl logs pod/admiralty-multicluster-scheduler-controller-manager-86bfcbw9n7n -n admiralty
I0506 13:05:48.384184       1 request.go:621] Throttling request took 1.043122228s, request: GET:https://10.40.0.1:443/apis/nodemanagement.gke.io/v1alpha1?timeout=32s
I0506 13:05:49.797554       1 controller.go:67] Starting source controller
I0506 13:05:49.797655       1 controller.go:70] Waiting for informer caches to sync
I0506 13:05:49.800328       1 controller.go:67] Starting ingresses-follow controller
I0506 13:05:49.800569       1 controller.go:70] Waiting for informer caches to sync
I0506 13:05:49.810350       1 controller.go:67] Starting chaperon controller
I0506 13:05:49.810606       1 controller.go:67] Starting cluster-resources-downstream controller
I0506 13:05:49.810761       1 controller.go:67] Starting cluster-resources-upstream controller
I0506 13:05:49.810875       1 controller.go:67] Starting feedback controller
I0506 13:05:49.810969       1 controller.go:67] Starting config-maps-follow controller
I0506 13:05:49.811071       1 controller.go:67] Starting services-follow controller
I0506 13:05:49.811152       1 controller.go:67] Starting secrets-follow controller
I0506 13:05:49.812520       1 controller.go:70] Waiting for informer caches to sync
I0506 13:05:49.812746       1 controller.go:70] Waiting for informer caches to sync
I0506 13:05:49.812922       1 controller.go:70] Waiting for informer caches to sync
I0506 13:05:49.813093       1 controller.go:70] Waiting for informer caches to sync
I0506 13:05:49.813250       1 controller.go:70] Waiting for informer caches to sync
I0506 13:05:49.813397       1 controller.go:70] Waiting for informer caches to sync
I0506 13:05:49.813548       1 controller.go:70] Waiting for informer caches to sync
I0506 13:05:49.897868       1 controller.go:75] Starting workers
I0506 13:05:49.897909       1 controller.go:81] Started workers
I0506 13:05:49.901788       1 controller.go:75] Starting workers
I0506 13:05:49.901831       1 controller.go:81] Started workers
I0506 13:05:49.912940       1 controller.go:75] Starting workers
I0506 13:05:49.912996       1 controller.go:81] Started workers
I0506 13:05:49.913067       1 controller.go:75] Starting workers
I0506 13:05:49.913080       1 controller.go:81] Started workers
I0506 13:05:49.913094       1 controller.go:75] Starting workers
I0506 13:05:49.913122       1 controller.go:81] Started workers
I0506 13:05:49.913332       1 controller.go:75] Starting workers
I0506 13:05:49.913346       1 controller.go:81] Started workers
I0506 13:05:49.914010       1 controller.go:75] Starting workers
I0506 13:05:49.914485       1 controller.go:81] Started workers
I0506 13:05:49.914140       1 controller.go:75] Starting workers
I0506 13:05:49.915196       1 controller.go:81] Started workers
I0506 13:05:49.914165       1 controller.go:75] Starting workers
I0506 13:05:49.915735       1 controller.go:81] Started workers
```

Sometimes, you might see some error lines, such as
```bash
E0506 12:00:12.050206       1 reflector.go:178] k8s.io/client-go@v0.18.9/tools/cache/reflector.go:125: Failed to list *v1beta1.Ingress: ingresses.networking.k8s.io is forbidden: User "system:serviceaccount:kc:cd" cannot list resource "ingresses" in API group "networking.k8s.io" in the namespace "default"
```
This means that there is an access right problem, and you should probably check the [RBAC](https://kubernetes.io/docs/reference/access-authn-authz/rbac/).
