# Admiralty multicluster-scheduler 

This is a benchmark on Admiralty. 
The goal is to test Admiralty with a workflow engine. We use Argo as workflow engine, since it is natively compatible
with Admiralty. 

## Versions used
This POC has been made using the following versions:
* Kubernetes: V1.18.17-gke.700 (GCP)
* Admiralty: V0.13.2
* cert-manager: V0.16.1
* Argo: V2.2.1

**NOTE: There is a problem with the version 1.19.XX of Kubernetes. The pods stay stuck in `Not Ready` state after the
execution.**

## Status 
The status today is:
* Admiralty is working as a multicluster-scheduler
* Admiralty can schedule workflow with argo on multi-cluster
* Persistent volumes such as GCS buckets can be used in combination with Admiralty and Argo
* Use heml charts to deploy RBAC resources

Here is a list of what needs to be done:
- [ ] Translate bash script to go
- [ ] Deploy on SB infra
- [ ] Deploy on Mundi
- [ ] Use buckets from outside fo GCP
- [ ] Use Amazon S3
- [ ] Use git as permanent volume


***
## Language used
Everything is scripted in bash. This is the quick and dirty solution
in order to get a working proof of concept. Helm charts have been implemented.
We might want to implement everything later in Go. 

***

## Get started
If you want to test Admiralty with standard kubernetes clusters, go to our
[Get started with admiralty](get_started_with_admiralty.md) page to set 
Admiralty on your kubernetes clusters.

If you use [kubernetes in docker (KIND)](https://kind.sigs.k8s.io/) check our 
[dedicated KIND guide](argo-kind-mcs/README.MD).


## Additional information

Here are some documentation regarding several tests that have been made.
* [Admiralty with Argo](./argo_gcp)
* [Test on GCP](./gcp)
* [Use Google Cloud Storage (GCS) bucket with Admiralty](./gcp/buckets.md)
* [Tests on `PersistentVolume`s](./benchmark_persistent_volumes.md)


