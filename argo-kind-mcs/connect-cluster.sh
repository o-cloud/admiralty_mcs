if [ $# -eq 0 ] 
then
    echo "Cannot connect less then 2 clusters"
    echo "Usage : ./connect-cluster.sh eu,am,as"
    exit 1
fi
CLUSTERS=(${1//,/ })
cluster_len=${#CLUSTERS[@]}
if [ $cluster_len -le 1 ] 
then
    echo "Cannot connect less then 2 clusters"
    echo "Usage : ./connect-cluster.sh eu,am,as"
    exit 1
fi

for CLUSTER_SOURCE in "${CLUSTERS[@]}"
do
    for CLUSTER_TARGET in "${CLUSTERS[@]}"
    do
        if [ $CLUSTER_SOURCE = $CLUSTER_TARGET ]
        then
            continue
        fi
        #auth
        kubectl --context kind-$CLUSTER_TARGET create serviceaccount $CLUSTER_SOURCE

        SECRET_NAME=$(kubectl --context kind-$CLUSTER_TARGET get serviceaccount $CLUSTER_SOURCE \
        --output json | \
        jq -r '.secrets[0].name')
        TOKEN=$(kubectl --context kind-$CLUSTER_TARGET get secret $SECRET_NAME \
        --output json | \
        jq -r '.data.token' | \
        base64 -i --decode)

        IP=$(docker inspect $CLUSTER_TARGET-control-plane \
        --format "{{ .NetworkSettings.Networks.kind.IPAddress }}")

        CONFIG=$(kubectl --context kind-$CLUSTER_TARGET config view \
        --minify --raw --output json | \
        jq '.users[0].user={token:"'$TOKEN'"} | .clusters[0].cluster.server="https://'$IP':6443"')

        kubectl --context kind-$CLUSTER_SOURCE create secret generic $CLUSTER_TARGET \
        --from-literal=config="$CONFIG"

        #Add source and target objects
        cat <<EOF | kubectl --context kind-$CLUSTER_SOURCE apply -f -
apiVersion: multicluster.admiralty.io/v1alpha1
kind: Target
metadata:
    name: $CLUSTER_TARGET
spec:
    kubeconfigSecret:
        name: $CLUSTER_TARGET
EOF

        cat <<EOF | kubectl --context kind-$CLUSTER_TARGET apply -f -
apiVersion: multicluster.admiralty.io/v1alpha1
kind: Source
metadata:
    name: $CLUSTER_SOURCE
spec:
    serviceAccountName: $CLUSTER_SOURCE
EOF

    done
done
