if [ $# -eq 0 ] 
then
    echo "You must specify the name of the cluster"
    echo "Usage : ./create-cluster.sh eu"
    exit 1
fi

CLUSTER_NAME=$1

#create cluster
kind create cluster --name $CLUSTER_NAME

#load images
images=(
  # cert-manager dependency
  quay.io/jetstack/cert-manager-controller:v0.16.1
  quay.io/jetstack/cert-manager-webhook:v0.16.1
  quay.io/jetstack/cert-manager-cainjector:v0.16.1
  # admiralty open source
  quay.io/admiralty/multicluster-scheduler-agent:0.13.2
  quay.io/admiralty/multicluster-scheduler-scheduler:0.13.2
  quay.io/admiralty/multicluster-scheduler-remove-finalizers:0.13.2
  quay.io/admiralty/multicluster-scheduler-restarter:0.13.2
  # admiralty cloud/enterprise
#   quay.io/admiralty/admiralty-cloud-controller-manager:0.13.2
#   quay.io/admiralty/kube-mtls-proxy:0.10.0
#   quay.io/admiralty/kube-oidc-proxy:v0.3.0 # jetstack's image rebuilt for multiple architectures
)
for image in "${images[@]}"
do
    kind load docker-image $image --name $CLUSTER_NAME
done

kubectl --context kind-$CLUSTER_NAME label nodes --all topology.kubernetes.io/region=$CLUSTER_NAME

#configure cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
kubectl --context kind-$CLUSTER_NAME create namespace cert-manager
kubectl --context kind-$CLUSTER_NAME apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.16.1/cert-manager.crds.yaml
helm install cert-manager jetstack/cert-manager \
--kube-context kind-$CLUSTER_NAME \
--namespace cert-manager \
--version v0.16.1 \
--wait --debug
  # --wait to ensure release is ready before next steps
  # --debug to show progress, for lack of a better way,
  # as this may take a few minutes

install admiralty
helm repo add admiralty https://charts.admiralty.io
helm repo update

kubectl --context kind-$CLUSTER_NAME create namespace admiralty
helm install admiralty admiralty/multicluster-scheduler \
--kube-context kind-$CLUSTER_NAME \
--namespace admiralty \
--version 0.13.2 \
--wait --debug
# --wait to ensure release is ready before next steps
# --debug to show progress, for lack of a better way,
# as this may take a few minutes

#auth
# i.
kubectl --context kind-$CLUSTER_NAME create serviceaccount $CLUSTER_NAME

# ii.
SECRET_NAME=$(kubectl --context kind-$CLUSTER_NAME get serviceaccount $CLUSTER_NAME \
--output json | \
jq -r '.secrets[0].name')
TOKEN=$(kubectl --context kind-$CLUSTER_NAME get secret $SECRET_NAME \
--output json | \
jq -r '.data.token' | \
base64 -i --decode)

# iii.
IP=$(docker inspect $CLUSTER_NAME-control-plane \
--format "{{ .NetworkSettings.Networks.kind.IPAddress }}")

# iv.
CONFIG=$(kubectl --context kind-$CLUSTER_NAME config view \
--minify --raw --output json | \
jq '.users[0].user={token:"'$TOKEN'"} | .clusters[0].cluster.server="https://'$IP':6443"')

# v.
kubectl --context kind-$CLUSTER_NAME create secret generic $CLUSTER_NAME \
--from-literal=config="$CONFIG"

#add self as target
cat <<EOF | kubectl --context kind-$CLUSTER_NAME apply -f -
apiVersion: multicluster.admiralty.io/v1alpha1
kind: Target
metadata:
  name: $CLUSTER_NAME
spec:
  kubeconfigSecret:
    name: $CLUSTER_NAME
EOF

#add self as source
cat <<EOF | kubectl --context kind-$CLUSTER_NAME apply -f -
apiVersion: multicluster.admiralty.io/v1alpha1
kind: Source
metadata:
  name: $CLUSTER_NAME
spec:
  serviceAccountName: $CLUSTER_NAME
EOF

kubectl --context kind-$CLUSTER_NAME label ns default multicluster-scheduler=enabled

#Install argo
#kubectl --context kind-$CLUSTER_NAME create namespace argo
#kubectl --context kind-$CLUSTER_NAME apply -n argo -f https://raw.githubusercontent.com/argoproj/argo-workflows/stable/manifests/install.yaml
kubectl --context kind-$CLUSTER_NAME apply -f https://raw.githubusercontent.com/argoproj/argo-workflows/stable/manifests/quick-start-postgres.yaml
#Update argo config to work in kind
kubectl --context kind-$CLUSTER_NAME get configmap workflow-controller-configmap -o json | jq '.data.containerRuntimeExecutor = "k8sapi"' | kubectl --context kind-$CLUSTER_NAME apply -f -
