if [ $# -eq 0 ] 
then
    echo "You must specify the name of the cluster"
    echo "Usage : ./create-cluster.sh eu"
    exit 1
fi

CLUSTER_NAME=$1

minikube start -p $CLUSTER_NAME --embed-certs
kubectl --context $CLUSTER_NAME label nodes --all topology.kubernetes.io/region=$CLUSTER_NAME

#configure cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
kubectl --context $CLUSTER_NAME create namespace cert-manager
kubectl --context $CLUSTER_NAME apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.16.1/cert-manager.crds.yaml
helm install cert-manager jetstack/cert-manager \
--kube-context $CLUSTER_NAME \
--namespace cert-manager \
--version v0.16.1 \
--wait --debug
  # --wait to ensure release is ready before next steps
  # --debug to show progress, for lack of a better way,
  # as this may take a few minutes

install admiralty
helm repo add admiralty https://charts.admiralty.io
helm repo update

kubectl --context $CLUSTER_NAME create namespace admiralty
helm install admiralty admiralty/multicluster-scheduler \
--kube-context $CLUSTER_NAME \
--namespace admiralty \
--version 0.13.2 \
--wait --debug
# --wait to ensure release is ready before next steps
# --debug to show progress, for lack of a better way,
# as this may take a few minutes

#auth
# i.
kubectl --context $CLUSTER_NAME create serviceaccount $CLUSTER_NAME

# ii.
SECRET_NAME=$(kubectl --context $CLUSTER_NAME get serviceaccount $CLUSTER_NAME \
--output json | \
jq -r '.secrets[0].name')
TOKEN=$(kubectl --context $CLUSTER_NAME get secret $SECRET_NAME \
--output json | \
jq -r '.data.token' | \
base64 -i --decode)

# iii.
IP=$(kubectl --context $CLUSTER_NAME -n kube-system get pods -o wide | grep kube-proxy | awk -F ' ' '{print $6}')
echo IP : $IP
# iv.
CONFIG=$(kubectl --context $CLUSTER_NAME config view \
--minify --raw --output json | \
jq '.users[0].user={token:"'$TOKEN'"} | .clusters[0].cluster.server="https://'$IP':6443"')

# v.
kubectl --context $CLUSTER_NAME create secret generic $CLUSTER_NAME \
--from-literal=config="$CONFIG"

#add self as target
cat <<EOF | kubectl --context $CLUSTER_NAME apply -f -
apiVersion: multicluster.admiralty.io/v1alpha1
kind: Target
metadata:
  name: $CLUSTER_NAME
spec:
  kubeconfigSecret:
    name: $CLUSTER_NAME
EOF

#add self as source
cat <<EOF | kubectl --context $CLUSTER_NAME apply -f -
apiVersion: multicluster.admiralty.io/v1alpha1
kind: Source
metadata:
  name: $CLUSTER_NAME
spec:
  serviceAccountName: $CLUSTER_NAME
EOF

kubectl --context $CLUSTER_NAME label ns default multicluster-scheduler=enabled

#Install argo
kubectl --context $CLUSTER_NAME create namespace argo
kubectl --context $CLUSTER_NAME apply -n argo -f https://raw.githubusercontent.com/argoproj/argo-workflows/stable/manifests/quick-start-postgres.yaml
kubectl --context $CLUSTER_NAME label ns argo multicluster-scheduler=enabled
