# Argo and multicluster scheduler
Here are several scripts to setup several kubernetes clusters using kind, install Multicluster Scheduler (MSC) on them, and run argo workflow on the clusters. On my laptop I could only get two clusters running.  

The network topology is [decentralized federation](https://admiralty.io/docs/concepts/topologies#decentralized-federation), each cluster can start pods on itself and each other cluster. This is achieved by adding each cluster as a source and a target to itself.

Due to errors with permissions and namespaces argo will be installed in the default namespace and every workflow needs to be started in the default namespace.

TODO:
  * try to specify on which cluster each task of the workflow should run
  * try to pass data between two tasks on distinct clusters
  * try to share volume between two tasks on distinct clusters

https://github.com/argoproj/argo-workflows
https://github.com/admiraltyio/admiralty
https://kind.sigs.k8s.io/docs/user/quick-start/

# Scripts
## create-network.sh
This script creates a network of several clusters, installs argo and msc, connects each clusters to the others.
This script calls create-cluster.sh and connect-cluster.sh
Usage: `./create-network.sh cluster-name-1,cluster-name-2,cluster-name-3`

## create-cluster.sh
Creates one cluster, installs argo and msc, connect it to itself
Usage: `./create-cluster.sh cluster-name`

## connect-cluster.sh
Connect all the clusters passed as argument (i.e. adds serviceaccounts, targets, sources)
Usage: `./connect-cluster.sh cluster-name-1,cluster-name-2,cluster-name-3`

## spawn-jobs.sh
Spawns dummy jobs on a cluster, this doesn't use argo but shows that each job can run on a distinct cluster
Usage: `./spawn-jobs.sh cluster-name`

## create-cluster-minikube.sh
Garbage.
Tried to get msc working on minikube. It doesn't work.

# Starting a workflow
run `argo --context kind-eu submit --watch argo-workflow.yaml` assuming you named your cluster `eu`
In each template with a container we can specify that mcs should manage the pod by adding metadata
```
      metadata:
        annotations:
          multicluster.admiralty.io/elect: ""
```

We are able to specify on which cluster each pod needs to run by adding a node selector somewhere.
```
      nodeSelector:
        topology.kubernetes.io/region: eu
```
# Communication between pods
Argo offers three ways to pass data between the different pods of the workflow
## Passing arguments
The simplest way. This works even if the pods run on different clusters, but using only arguments is limiting.
We can pass the output of a pod as a parameter to another pod.
See file `workflow/workflow-output-param.yaml`. Here we need to use [emptyDir](https://argoproj.github.io/argo-workflows/empty-dir/) because of kind

## Using an artifact repository
Pods can read and write to an [artifact repository](https://argoproj.github.io/argo-workflows/configure-artifact-repository/). An artifact repository is an S3 compatible storage.
Artifact repository can be configured globally in a cluster or independently in a workflow definition.

## Using volumes
We can always mount volumes in the pods. The claim needs to be present on every cluster where the workflow will run.

# Problems encountered 
## Running argo in kind cluster
Kind doesn't use docker to run pods, it uses containerd so we need to update `workflow-controller-configmap` in `argo` namespace

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: workflow-controller-configmap
data:
  # https://argoproj.github.io/argo-workflows/workflow-controller-configmap.yaml

  # Specifies the container runtime interface to use (default: docker)
  # must be one of: docker, kubelet, k8sapi, pns
  # It has lower precedence than either `--container-runtime-executor` and `containerRuntimeExecutors`.
  #
  # This is necessary for kind
  containerRuntimeExecutor: k8sapi
```

https://argoproj.github.io/argo-workflows/workflow-executors/
https://github.com/argoproj/argo-workflows/issues/5243
https://kind.sigs.k8s.io/

## Argo only starts workflow in argo namespace

The installation with quickstart postgres is a namespaced one
To enable workflow in other namespaces we need to edit the workflow-controller and argo server deployments 
For exemple :
```
      - args:
        - --namespaced
        - --managed-namespace
        - default
```
The `managed-namespace` argument allows execution of workflows in other namespaces
https://argoproj.github.io/argo-workflows/installation/
https://argoproj.github.io/argo-workflows/managed-namespace/

Ca a pas l'air de fonctionner ...

A helm project also exists
https://github.com/argoproj/argo-helm

## MCS doesn't work in argo ns
` Unschedulable: 0/2 nodes are available: 1 node(s) didn't match node selector, 1 podchaperons.multicluster.admiralty.io is forbidden: User "system:serviceaccount:default:eu" cannot list resource "podchaperons" in API group "multicluster.admiralty.io" in the namespace "argo".`

Service account eu doesn't exists in argo ns ?

## Argo error during workflow execution `failed to save outputs: Failed to establish pod watch: timed out waiting for the condition`
Occurs when running a workflow in the default ns after installing argo with the install yaml
