if [ $# -eq 0 ] 
then
    echo "You must specify the source cluster"
    echo "Usage : ./spawn-jobs.sh eu"
    exit 1
fi

CLUSTER_NAME=$1

for i in $(seq 1 10)
do
  cat <<EOF | kubectl --context kind-$CLUSTER_NAME -n argo apply -f -
apiVersion: batch/v1
kind: Job
metadata:
  name: global-$i
spec:
  template:
    metadata:
      annotations:
        multicluster.admiralty.io/elect: ""
    spec:
      containers:
      - name: c
        image: busybox
        command: ["sh", "-c", "echo Processing item $i && sleep 5"]
        resources:
          requests:
            cpu: 100m
      restartPolicy: Never
EOF
done
