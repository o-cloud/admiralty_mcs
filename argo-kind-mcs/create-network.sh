if [ $# -eq 0 ] 
then
    echo "Cannot create a network with less then 2 clusters"
    echo "Usage : ./create-network.sh eu,am,as"
    exit 1
fi
CLUSTERS=(${1//,/ })
cluster_len=${#CLUSTERS[@]}
if [ $cluster_len -le 1 ] 
then
    echo "Cannot create a network with less then 2 clusters"
    echo "Usage : ./create-network.sh eu,am,as"
    exit 1
fi

for CLUSTER in "${CLUSTERS[@]}"
do
    ./create-cluster.sh $CLUSTER
done

./connect-cluster.sh $1