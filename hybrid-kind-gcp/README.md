# Multicluster-scheduler tests on Kind-GCP

This is a small test running  Multicluster-scheduler on the kind and GCP.

The management cluster is set locally with kind `kind-cd`, and the
target cluster is set on GCP `gke_irt-sb_europe-west2-b_sb-simon-1`.

You will have to replace these cluster names in the scripts by the name of the clusters you
are using.

First set and execute the `installation.sh` script.
```bash
sh installation.sh
```
This script will install `cert-manager`, update the helm repos and `admiralty` on the management and the target clusters.

Then execute the `mcs_create_secrets_token.sh` script.
```bash
mcs_create_secrets_token.sh
```
This script will:
* Create a service account on the target cluster.
* Extract a token for that service account.
* Extract the IP of the API server on the target cluster.
* Use the above information to create secret of the management cluster.


Then execute the `create_target_source_cluster.sh`script
```bash
sh create_target_source_cluster.sh
```
That script will create a target cluster in the management cluster and a management cluster
in the target cluster.
It will also label the default namespace of the source cluster with `multicluster-scheduler=enabled` and set the context to the management cluster.

You can now create jobs with the `create_kubernetes_jobs.sh`
```bash
sh create_kubernetes_jobs.sh
```
That script will create a serie of jobs ob the management cluster. These jobs are annotated with `multicluster.admiralty.io/elect:""` so they will be deployed on the target clusters.


You can run the `check_pods.sh` script to the check the status of the pods, both on the management and the target cluster.

Once you are done, you can clean everything with the `clean.sh` script.


* installation.sh
* mcs_create_secrets_token.sh
* create_target_source_cluster.sh
* create_kubernetes_jobs.sh
* check_pods.sh
