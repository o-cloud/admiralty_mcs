#!/bin/sh

# Install cert manager on all cluster

for CLUSTER_NAME in kind-cd gke_irt-sb_europe-west2-b_sb-simon-1
do
	kubectl config use-context $CLUSTER_NAME
	kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.2.0/cert-manager.yaml

	helm install cert-manager jetstack/cert-manager \
        	--kube-context $CLUSTER_NAME \
    		--namespace cert-manager \
    		--version v0.16.1 \
    		--wait --debug
done



# Add admiralty repo and update
helm repo add admiralty https://charts.admiralty.io
helm repo update

#install admiralty on all clusters 

for CLUSTER_NAME in kind-cd gke_irt-sb_europe-west2-b_sb-simon-1
do
	kubectl --context $CLUSTER_NAME create namespace admiralty	

	helm install admiralty admiralty/multicluster-scheduler \
    		--kube-context $CLUSTER_NAME \
    		--namespace admiralty \
    		--version 0.13.2 \
    		--wait --debug
done
