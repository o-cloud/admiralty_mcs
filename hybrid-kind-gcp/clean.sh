#!/bin/sh

for CLUSTER_NAME in kind-cd gke_irt-sb_europe-west1-b_sb-simon-1
do
	for NAMESPACE in default admiralty cert-manager
	do
		#kubectl --context $CLUSTER_NAME delete pods --all -n $NAMESPACE
		kubectl --context $CLUSTER_NAME delete jobs --all -n $NAMESPACE
		kubectl --context $CLUSTER_NAME delete secrets --all -n $NAMESPACE
		kubectl --context $CLUSTER_NAME delete serviceaccounts --all
		kubectl --context $CLUSTER_NAME delete serviceaccounts cd
		kubectl --context $CLUSTER_NAME delete serviceaccounts --all -n $NAMESPACE
		kubectl --context $CLUSTER_NAME delete namespaces $NAMESPACE
	done

	kubectl --context $CLUSTER_NAME delete namespaces --all
	helm uninstall cert-manager -n certmanager
	helm uninstall admiralty -n admiralty
	

done
