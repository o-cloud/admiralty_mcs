#!/bin/sh

for CLUSTER_NAME in gke_irt-sb_europe-west2-b_sb-simon-1
do
  cat <<EOF | kubectl --context kind-cd apply -f -
apiVersion: multicluster.admiralty.io/v1alpha1
kind: Target
metadata:
  name: mysecret
spec:
  kubeconfigSecret:
    name: mysecret
EOF
done

for CLUSTER_NAME in gke_irt-sb_europe-west2-b_sb-simon-1
do
  cat <<EOF | kubectl --context $CLUSTER_NAME apply -f -
apiVersion: multicluster.admiralty.io/v1alpha1
kind: Source
metadata:
  name: cd
spec:
  serviceAccountName: cd
EOF
done

sleep 20
kubectl --context kind-cd label ns default multicluster-scheduler=enabled
kubectl config use-context kind-cd
