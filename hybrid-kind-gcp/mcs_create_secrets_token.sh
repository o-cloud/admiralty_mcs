#!/bin/sh

# Configure multicluster-scheduler. 
# This is done on the target clusters.

MANAGEMENT_CLUSTER="kind-cd"

for CLUSTER_NAME in gke_irt-sb_europe-west2-b_sb-simon-1
do
  echo "Setting cluster" $CLUSTER_NAME
  # i.
  kubectl --context $CLUSTER_NAME create serviceaccount cd

  # ii.
  SECRET_NAME=$(kubectl --context $CLUSTER_NAME get serviceaccount cd \
    --output json | \
    jq -r '.secrets[0].name')

  TOKEN=$(kubectl --context $CLUSTER_NAME get secret $SECRET_NAME \
    --output json | \
    jq -r '.data.token' | \
    base64 --decode)

  echo "TOKEN" $TOKEN


  # Retrieve IP address from API server
  #IP="34.77.84.31"
  IP=$(kubectl --context $CLUSTER_NAME cluster-info dump --output json | jq -r '.items[3].spec.template.spec.containers[0].env[0].value' | grep -v null) # -> timeout

 echo "IP address is" $IP

  # iv.
  CONFIG=$(kubectl --context $CLUSTER_NAME config view \
    --minify --raw --output json | \
    jq '.users[0].user={token:"'$TOKEN'"} | .clusters[0].cluster.server="https://'$IP':443"')
  echo $CONFIG

  # v.
  kubectl --context $MANAGEMENT_CLUSTER create secret generic mysecret \
    --from-literal=config="$CONFIG"
done
