#!/bin/sh
while true
do
  clear
  for CLUSTER_NAME in kind-cd gke_irt-sb_europe-west2-b_sb-simon-1
  do
    kubectl --context $CLUSTER_NAME get pods -o wide 
  done
  sleep 2
done
