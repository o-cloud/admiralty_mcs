# Helm in SB project

Here is a small test to use helm charts. This is better than hardcoding
the manifest in the bash scripts.

First step is to use helm chart to deploy the Target, Source, ClusterRoles and ClusterRoleBinding
resources.

We created a helm chart for the Sources clusters and another one for the target clusters. This is motivated
by the fact that the `.yaml` file is distinc for both ressources and overwritting variables was not enough.

## Links
* [Override values in helm chart](https://v3-1-0.helm.sh/docs/helm/helm_install/)
