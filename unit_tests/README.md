# Unit test 
This is a small list and set of script to make small tests in order
to test the infrastructure.

## PersistentVolumes
```bash
kubectl apply -f test_pvc_gcp.yaml
```
This should create a file  where the persisten volume in mounted `/mnt/data/test.txt` and write into it.

## Argo
Basic Argo workflow
```bash
argo submit -n argo --watch argo_helloworld.yaml
```

Test on `PersistentVolumes` mounted in argo
```bash
argo submit -n argo argo_test-pvc.yaml
```

## Admiralty
Deploy a basic jobs using multicluster-scheduler
```bash
sh create_admiralty_jobs.sh
```
