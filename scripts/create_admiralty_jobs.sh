#!/bin/sh

source config.sh

for i in $(seq 1 5)
do
  cat <<EOF | kubectl --context $SOURCE_CLUSTER  -n $NAMESPACE_SOURCE apply -f -
apiVersion: batch/v1
kind: Job
metadata:
  name: global-$i
  labels: 
    application: admiralty
spec:
  template:
    metadata:
      annotations:
        multicluster.admiralty.io/elect: ""
    spec:
      containers:
      - name: c
        image: busybox
        command: ["sh", "-c", "echo Processing item $i && sleep 5"]
        resources:
          requests:
            cpu: 100m
      restartPolicy: Never
EOF
done
