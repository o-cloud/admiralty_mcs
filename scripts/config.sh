# Clusters to use
# Set the TARGET_CLUSTER as follow if you have serveral target clusters:
# TARGET_CLUSTER="cluster1 cluster2 cluster3"

SOURCE_CLUSTER="gke_irt-sb_us-central1-c_cluster-sbo-management"
TARGET_CLUSTER="gke_irt-sb_us-central1-c_cluster-sbo-2"

# Set the namespaces to be used by each cluster
NAMESPACE_TARGET=kc
NAMESPACE_SOURCE=default

#SERVICEACCOUNT_TARGET=cd

# Kubernetes API server ports
# This is usually port 6443, however on GCP you need
# to set it to 443
api_server_port=443

# Set to 1 if you are using a kind cluster
IS_KIND=0
