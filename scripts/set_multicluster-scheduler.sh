#!/bin/sh

admiralty_installation() {
	# Installation of cert-manager and admiralty on the clusters.

	# Install cert manager on all cluster
	for CLUSTER_NAME in $TARGET_CLUSTER $SOURCE_CLUSTER
	do
		kubectl config use-context $CLUSTER_NAME
		kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.2.0/cert-manager.yaml
	
		helm install cert-manager jetstack/cert-manager \
			--kube-context $CLUSTER_NAME \
			    --namespace cert-manager \
			    --version v0.16.1 \
			    --wait --debug

		kubectl --context $CLUSTER_NAME label namespace cert-manager application=admiralty
	done
	
	
	
	# When using kind, we have to add a 20 sec sleep, otherwise there is a problem
	# when install admiralty chart
	if [ "$IS_KIND" -eq 1 ]
	then
		sleep 20
	
	fi
	
	
	# Add admiralty repo and update
	helm repo add admiralty https://charts.admiralty.io
	helm repo update
	
	#install admiralty on all clusters 
	
	for CLUSTER_NAME in $TARGET_CLUSTER $SOURCE_CLUSTER
	do
		kubectl --context $CLUSTER_NAME create namespace admiralty	
	
		helm install admiralty admiralty/multicluster-scheduler \
			    --kube-context $CLUSTER_NAME \
			    --namespace admiralty \
			    --version 0.13.2 \
			    --wait --debug
		
		kubectl --context $CLUSTER_NAME label namespace admiralty application=admiralty
	done
}

set_regions(){
#######################################
#
# This function is outdated. It was used for tests
#
#######################################

# Label each cluster with a region
	var=0
	for CLUSTER_NAME in $TARGET_CLUSTER
	do
		echo $var
		if [ "$var" -eq 0 ]
		then	
			#echo $CLUSTER_NAME
			  kubectl --context $CLUSTER_NAME label nodes \
				  --all topology.kubernetes.io/region=eu
	
	        elif [ "$var" -eq 1 ]
		then	
			#echo $CLUSTER_NAME
			  kubectl --context $CLUSTER_NAME label nodes \
				  --all topology.kubernetes.io/region=us
		fi	
		var=$(( var+1 ))
	done
}

create_secret() {
	# This function creates a service account for the target. 
	# It then creates a secret on the managment server, from the serviceaccount created on the
        # target cluster in order to connect to the API server on the target cluster


	for CLUSTER_NAME in $TARGET_CLUSTER
	do
	  # Create dedicated namespace
	  kubectl --context $CLUSTER_NAME create namespace $NAMESPACE_TARGET

	  # Label the namespace
	  kubectl --context $CLUSTER_NAME label namespace $NAMESPACE_TARGET  application=admiralty

	  # create service account
	  kubectl --context $CLUSTER_NAME -n $NAMESPACE_TARGET create serviceaccount admiralty-auth-sa
	  kubectl --context $CLUSTER_NAME label serviceaccount admiralty-auth-sa  application=admiralty
	
	   
	  # Extract the secret from the service account
	  SECRET_NAME=$(kubectl --context $CLUSTER_NAME -n $NAMESPACE_TARGET get serviceaccount admiralty-auth-sa \
	    --output json | \
	    jq -r '.secrets[0].name')
	
	  # Retrieve the token from the secret extracted
	  TOKEN=$(kubectl --context $CLUSTER_NAME -n $NAMESPACE_TARGET get secret $SECRET_NAME \
	    --output json | \
	    jq -r '.data.token' | \
	    base64 --decode)
	
	
	  # Retrieve IP address from API server
	  IP=$(kubectl --context $CLUSTER_NAME cluster-info dump --output json \
		  | jq -r '.items[3].spec.template.spec.containers[0].env[0].value' \
		  | grep -v null) 


	  if [ "$IS_KIND" -eq 1 ]
	  then
		 # We remove some garbage that kind adds to the cluster name
		 CLUSTER=$(echo $CLUSTER_NAME | sed 's/kind-//')

		 # Retrieve the IP via docker
		 IP=$(docker inspect eu-control-plane \
    		--format "{{ .NetworkSettings.Networks.kind.IPAddress }}")  
	  fi

	
	  # Get length of the IP address to check validity
	  # This step is required when working with GCP workload identity
	  IP_LENGTH=`echo $IP | wc -c`

	  # IP_LIMIT to check if IP is valid
	  IP_LIMIT=8
	  
	  # Check if the IP is valid. It might not be valid if Workload Identity is enabled
	  # on GCP.
	  if [ $IP_LENGTH -lt $IP_LIMIT ]; then

		# If the IP is not valid, we retrieve it from another positioin
	  	IP=$(kubectl --context $CLUSTER_NAME cluster-info dump --output json \
		  | jq -r '.items[4].spec.template.spec.containers[0].env[0].value' \
		  | grep -v null) 

		api_server_port=443
	  fi
	  
	 echo "IP address is" $IP
	
	 # Create configuration for secret
	  CONFIG=$(kubectl --context $CLUSTER_NAME config view \
	    --minify --raw --output json | \
	    jq '.users[0].user={token:"'$TOKEN'"} | .clusters[0].cluster.server="https://'$IP':'$api_server_port'"')
	  echo $CONFIG
	
	  # Created required namespace on management cluster
	  kubectl --context $SOURCE_CLUSTER create namespace $NAMESPACE_SOURCE

	  # Create secret on management cluster
	  SECRET_NAME="secret-"$(echo $CLUSTER_NAME | sed 's/_/-/g')
	  kubectl --context $SOURCE_CLUSTER -n $NAMESPACE_SOURCE create secret generic $SECRET_NAME \
	    --from-literal=config="$CONFIG"


	  kubectl --context $SOURCE_CLUSTER label secret $SECRET_NAME -n $NAMESPACE_SOURCE application=admiralty
	done
}

set_clusters() {
	
	#Check if the same namespace is used in the source and target clusters
	SOURCE_CLUSTER_TYPE="Source"
	IS_RBAC=0
	if [[ "$NAMESPACE_SOURCE" != "$NAMESPACE_TARGET" ]]; then
		echo "Specified namespaces are different for source and target clusters \n"

		# If namespaces are not the same, with need to set the RBAC and 
		# Create a ClusterSource instead of a Cluster
		#sh admiralty-rbac.sh
		SOURCE_CLUSTER_TYPE="ClusterSource"
		IS_RBAC=1
	fi

# Prepare the scheduling on all clusters
for CLUSTER_NAME in $TARGET_CLUSTER
do
  # We need to mirror the namespace used by the source cluster
  # on every target cluster
  kubectl --context $CLUSTER_NAME create namespace $NAMESPACE_SOURCE

  kubectl --context $CLUSTER_NAME label namespace $NAMESPACE_SOURCE application=admiralty

  # Set the secret name to the good/accepted format
  SECRET_NAME="secret-"$(echo $CLUSTER_NAME | sed 's/_/-/g')
  echo $SECRET_NAME

  # Create the Source and Target on the clusters
  # We also add random numbers to avoid having two installs with the same name

  echo "helmname" $HELM_TARGET
  #HELM_TARGET="target-"$(echo $CLUSTER_NAME | sed 's/_/-/g')$(echo ${RANDOM:0:4})
  HELM_TARGET="target-"$(echo $CLUSTER_NAME | sed 's/_/-/g' | awk '{ print substr($0,1,40) }')$(echo ${RANDOM:0:4})
  echo "helmname" $HELM_TARGET
  helm install $HELM_TARGET ../helm/admiralty_source \
		   --kube-context $SOURCE_CLUSTER \
		   --namespace $NAMESPACE_SOURCE \
		   --set admiralty_ressource_type=Target \
		   --set secret_name=$SECRET_NAME \
		   --debug


done

for CLUSTER_NAME in $TARGET_CLUSTER
do
  # HELM_SOURCE="source-"$(echo $SOURCE_CLUSTER | sed 's/_/-/g')

  # We need to be sure that the name of the release is not longer than 53 characters
  # We also add random numbers to avoid having two installs with the same name
  HELM_SOURCE="source-"$(echo $SOURCE_CLUSTER | sed 's/_/-/g' | awk '{ print substr($0,1,40) }')$(echo ${RANDOM:0:4})
 helm install $HELM_SOURCE ../helm/admiralty_target \
			   --kube-context $CLUSTER_NAME \
			   --namespace $NAMESPACE_TARGET \
			   --set admiralty_ressource_type=$SOURCE_CLUSTER_TYPE \
			   --set serviceaccount_target=admiralty-auth-sa \
			   --set namespace_target=$NAMESPACE_TARGET \
		           --set rbac=$IS_RBAC \
 			   --debug
done

# Label the namespace to be able to execute pods with Admiralty
kubectl --context $SOURCE_CLUSTER label ns $NAMESPACE_SOURCE multicluster-scheduler=enabled
kubectl --context $SOURCE_CLUSTER label ns $NAMESPACE_SOURCE application=admiralty
}


#################################
#
#   Main part of the script
#
#################################

# Source the config
source config.sh


# Add the RBAC to the target cluster
#kubectl --context $TARGET_CLUSTER apply -f admiralty-rbac.yaml

# Launch the various installation steps
admiralty_installation
create_secret
set_clusters
