#!/bin/sh

source config.sh

while true
do
  clear
  for CLUSTER_NAME in $MANAGEMENT_CLUSTER $TARGET_CLUSTER
  do
    kubectl --context $CLUSTER_NAME get pods -o wide -n $NAMESPACE_SOURCE
  done
  sleep 2
done
