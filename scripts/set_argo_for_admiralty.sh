source config.sh

CLUSTER1=$SOURCE_CLUSTER 
CLUSTER2=$TARGET_CLUSTER 

ARGO_CLUSTER=$CLUSTER1 
NON_ARGO_CLUSTER=$CLUSTER2 

kubectl --context $ARGO_CLUSTER create ns argo
kubectl --context $ARGO_CLUSTER label ns argo application=argo

kubectl --context $ARGO_CLUSTER apply -n argo -f https://raw.githubusercontent.com/argoproj/argo-workflows/stable/manifests/install.yaml # file from tutorial is outdated
# NOTE: We need to use the install.yaml provided by admiralty. The quick-start-postgres.yaml provided by Argo is not suited to work with Admiralty


ARGO_POD_RBAC=https://raw.githubusercontent.com/admiraltyio/admiralty/master/examples/argo-workflows/_service-account.yaml # file from tutorial is outdated
kubectl --context $ARGO_CLUSTER -n $NAMESPACE_SOURCE apply -f $ARGO_POD_RBAC
kubectl --context $ARGO_CLUSTER -n $NAMESPACE_SOURCE label serviceaccount argo-workflow application=argo

for CLUSTER in $NON_ARGO_CLUSTER
do
	kubectl --context $CLUSTER -n $NAMESPACE_SOURCE apply -f $ARGO_POD_RBAC
	kubectl --context $CLUSTER -n $NAMESPACE_SOURCE label serviceaccount argo-workflow application=argo
done

# enable multicluster-scheduler on default namespace
kubectl --context $ARGO_CLUSTER label ns $NAMESPACE_SOURCE multicluster-scheduler=enabled

# Specifie the container runtime to use. From kubernetes V1.19, docker is not working anymore. 
kubectl --context $SOURCE_CLUSTER get configmaps workflow-controller-configmap -n argo -o json | jq '.data.containerRuntimeExecutor = "k8sapi"' | kubectl --context $ARGO_CLUSTER apply -f - -n argo
