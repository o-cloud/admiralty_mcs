# This is a script to clean the cluster after some Admiralty/Argo installation.

#!/bin/sh

source config.sh

# Run over all the cluster

for CLUSTER_NAME in  $TARGET_CLUSTER $SOURCE_CLUSTER
do

	kubectl config use-context $CLUSTER_NAME
	for APPLICATION in admiralty argo
	do
	# We have to launch several commands to delete all resources labeled with a given application
	# Because if one of the resources is not found the command stops

		kubectl --context $CLUSTER_NAME delete namespaces -l application=$APPLICATION
		kubectl --context $CLUSTER_NAME delete jobs -l application=$APPLICATION
		kubectl --context $CLUSTER_NAME delete serviceaccounts -l application=$APPLICATION
		kubectl --context $CLUSTER_NAME delete clusterrole -l application=$APPLICATION
		kubectl --context $CLUSTER_NAME delete clusterrolebinding -l application=$APPLICATION
		kubectl --context $CLUSTER_NAME delete secrets -l application=$APPLICATION
		kubectl --context $CLUSTER_NAME delete Source -l application=$APPLICATION
		kubectl --context $CLUSTER_NAME delete ClusterSource -l application=$APPLICATION
		kubectl --context $CLUSTER_NAME delete Target -l application=$APPLICATION
		kubectl --context $CLUSTER_NAME delete ClusterTarget -l application=$APPLICATION
		kubectl --context $CLUSTER_NAME delete serviceaccounts --all
		kubectl --context $CLUSTER_NAME delete secrets --all
		kubectl --context $CLUSTER_NAME delete crd clusterworkflowtemplates.argoproj.io
		kubectl --context $CLUSTER_NAME delete crd cronworkflows.argoproj.io                        
		kubectl --context $CLUSTER_NAME delete crd workfloweventbindings.argoproj.io                
		kubectl --context $CLUSTER_NAME delete crd workflows.argoproj.io                            
		kubectl --context $CLUSTER_NAME delete crd workflowtemplates.argoproj.io                    
		kubectl --context $CLUSTER_NAME delete role argo-workflow
		kubectl --context $CLUSTER_NAME delete rolebinding argo-workflow
		kubectl --context $CLUSTER_NAME delete clusterrole argo-aggregate-to-admin

	done

	# Delete helm releases in namespace default

	helm del $(helm ls --all --short)
	
	# Delete virtual nodes
	# Here, we have to do it manually since there are created directly by admiralty, they are not labeled

	for NODE in `kubectl --context $CLUSTER_NAME get nodes | grep admiralty | awk -F ' ' ' { print $1 } '`
	do
		kubectl --context $CLUSTER_NAME delete node $NODE
	done

done
