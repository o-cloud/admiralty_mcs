for CLUSTER_NAME in us
do
  # i.
  kubectl --context kind-$CLUSTER_NAME create serviceaccount cd

  # ii.
  SECRET_NAME=$(kubectl --context kind-$CLUSTER_NAME get serviceaccount cd \
    --output json | \
    jq -r '.secrets[0].name')
  TOKEN=$(kubectl --context kind-$CLUSTER_NAME get secret $SECRET_NAME \
    --output json | \
    jq -r '.data.token' | \
    base64 --decode)

  # iii.
  IP=$(docker inspect $CLUSTER_NAME-control-plane \
    --format "{{ .NetworkSettings.Networks.kind.IPAddress }}")

  # iv.
  CONFIG=$(kubectl --context kind-$CLUSTER_NAME config view \
    --minify --raw --output json | \
    jq '.users[0].user={token:"'$TOKEN'"} | .clusters[0].cluster.server="https://'$IP':6443"')

  # v.
  kubectl --context kind-cd create secret generic $CLUSTER_NAME \
    --from-literal=config="$CONFIG"
done
