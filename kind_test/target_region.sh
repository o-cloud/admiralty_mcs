for i in $(seq 1 10)
do
  cat <<EOF | kubectl --context kind-cd apply -f -
apiVersion: batch/v1
kind: Job
metadata:
  name: us-$i
spec:
  template:
    metadata:
      annotations:
        multicluster.admiralty.io/elect: ""
    spec:
      nodeSelector:
        topology.kubernetes.io/region: us
      containers:
      - name: c
        image: busybox
        command: ["sh", "-c", "echo Processing item $i && sleep 5"]
        resources:
          requests:
            cpu: 100m
      restartPolicy: Never
EOF
done
