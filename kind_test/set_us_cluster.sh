#!/bin/bash


CLUSTER_NAME="us"
#Create clusters
kind create cluster --name $CLUSTER_NAME

# label clusters
kubectl --context kind-$CLUSTER_NAME label nodes --all topology.kubernetes.io/region=$CLUSTER_NAME

# Install cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update

kubectl --context kind-$CLUSTER_NAME create namespace cert-manager
kubectl --context kind-$CLUSTER_NAME apply -f https://github.com/jetstack/cert-manager/releases/download/v1.2.0/cert-manager.yaml

helm install cert-manager jetstack/cert-manager \
  --kube-context kind-$CLUSTER_NAME \
  --namespace cert-manager \
  --version v0.16.1 \
  --wait --debug

# leave time for all pods to start
sleep 60

# Install admiralty

helm repo add admiralty https://charts.admiralty.io
helm repo update

kubectl --context kind-$CLUSTER_NAME create namespace admiralty
helm install admiralty admiralty/multicluster-scheduler \
	--kube-context kind-$CLUSTER_NAME \
	--namespace admiralty \
	--version 0.13.2 \
	--wait --debug

# Check that everythin is OK
kubectl --kube-context kind-$CLUSTER_NAME get all -n admiralty
