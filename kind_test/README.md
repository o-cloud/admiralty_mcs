# Run multicluster-scheduler on kind (kubernetes in docker)

Here are a bunch of scripts to run MCS with kind extracted from the official
![Admiralty documentation](https://admiralty.io/docs/quick_start/)

This is the order to launch the scripts for the test of MCS.

* set_cd_cluster.sh
* set_us_cluster.sh
* configuration_us.sh
* multicluster_scheduling.sh
* create_kubernetes_jobs.sh
* check_pods.sh
* target_region.sh

