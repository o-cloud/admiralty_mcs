# In the management cluster, create a Target for each workload cluster
for CLUSTER_NAME in us 
do
  cat <<EOF | kubectl --context kind-cd apply -f -
apiVersion: multicluster.admiralty.io/v1alpha1
kind: Target
metadata:
  name: $CLUSTER_NAME
spec:
  kubeconfigSecret:
    name: $CLUSTER_NAME
EOF
done

# In the workload clusters, create a Source for the management cluster
for CLUSTER_NAME in us
do
  cat <<EOF | kubectl --context kind-$CLUSTER_NAME apply -f -
apiVersion: multicluster.admiralty.io/v1alpha1
kind: Source
metadata:
  name: cd
spec:
  serviceAccountName: cd
EOF
done


# Label the default namespace in the management cluster to enable 
# multi-cluster scheduling at the namespace level:
kubectl --context kind-cd label ns default multicluster-scheduler=enabled
