# Argo artifacts on GCS using buckets
Here is a set of workflows that use Admiralty, Argo and GCS buckets.

The `input-output-artifact-gcs.yaml` is just a dummy example of intput/output using GCS buckets.
The `gdal_conversion.yaml` workflow uses the `gdal` docker image and does some dummy conversion on a image.
The `gdal_conversion_resize.yaml` workflow uses the `gdal` docker image and does some dummy conversion and compression on a image.
For the last two workflows, you will also need the `for_gcs.tar` archive.

**Also pay attention to replace the buckets set in the workflows by you own one**


## Troubleshooting
It seems that it is complicated to mount as input a folder that was created directly
from the GCS dashboard. That folder is always "interpreted" as a file.
However, if a file is created by a workflow that has mounted an
artifact as output, that file can later be mounted again and read. 

A first solution adopted to circumvent that problem is:
* Create a .tar file with all the files needed
* Use that file in the first artifacts and do whatever work you need to do with it.
* Output the results from the first stop in a new folder on the bucket.
* You can reuse that last bucket as a storage among all the further steps.
