# Set Argo worflow with Admiralty

Before starting you might want to take a look at the [Admiralty tutorial](https://admiralty.io/blog/2019/01/17/running-argo-workflows-across-multiple-kubernetes-clusters). 
Pay attention that some links might be dead.

Also, take a look at our local [get started with admiralty](get_started_with_admiralty.md) guide, to get Admiralty ready on your clusters 

## Install Argo
To deploy the Argo installation and set-up on all the clusters, you need the
[`set_argo_for_admiralty.sh`](../scripts/set_argo_for_admiralty.sh) script and
the [`config.sh`](../scripts/config.sh) file.
Pay attention to use the same `config.sh` file that you used to install Admiralty.

Install Argo:

```bash
sh set_argo_for_admiralty.sh
```

The script will source the `config.sh` and install Argo on the
management cluster. Note that a decicated manifest provided by Admiralty is used in order to 
get Argo running with Admiralty. Furthermore, a `serviceaccount` will be created on all the cluster, in order to prevent
Argo to run with admin priviledges on every cluster.

**Note that the files in the Admiralty tutorial for the installation of Argo are outdated!**

## Launch workflow
Several Argo workflow are provided. You can find them in the [argo-workflows](./argo-workflows) folder.
The workflows have to be submitted on the source cluster in the namespace set to use Admiralty.

### Single cluster

The first example is a single-cluster worfklow. It has to be launched on the management cluster.
```bash 
argo --context $MANAGEMENT_CLUSTER -n $NAMESPACE_SOURCE submit --serviceaccount argo-workflow --watch https://raw.githubusercontent.com/admiraltyio/admiralty/master/examples/argo-workflows/blog-scenario-a-singlecluster.yaml
```
You can at the same time launch the `check_pods.sh` script to follow the running pods
```bash
sh check_pods.sh
```

### Multi cluster
The following example will run the workflow annotating the worflow pod's template with `multicluster.admiralty.io/elect=""`, to make it run on two clusters.

```bash
argo submit --context $MANAGEMENT_CLUSTER -n $NAMESPACE_SOURCE --serviceaccount argo-workflow --watch https://raw.githubusercontent.com/admiraltyio/admiralty/master/examples/argo-workflows/blog-scenario-a-multicluster.yaml
```

### Multi cluster enforcing location
The last example runs Argo on multicluster forcing the location of the workflow execution.
For this example, you need to set the name of the target cluster in the .yaml file. This is done in the following commands. You need to have set the clusters in the environment variables for it to work properly.
```bash
curl -X GET "https://raw.githubusercontent.com/admiraltyio/admiralty/master/examples/argo-workflows/blog-scenario-b.yaml" | sed "s|cluster2|$NON_ARGO_CLUSTER|g" > blog-scenario-b.yaml
argo --context $ARGO_CLUSTER -n $NAMESPACE_SOURCE submit --serviceaccount argo-workflow --watch blog-scenario-b.yaml 
```

## Use of Artifacts with Argo
Argo uses artifacts to store data that can be used by several steps of the workflow. 
You can find below a list of artifacts that have been prepared to work with Argo:
* [GCS - Bucket](./artifacts/gcs-bucket.md)

