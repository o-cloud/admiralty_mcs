# How to use artifacts with Argo

Artifacts are used to store data that are to be fed to workflows or save output.
Check the [Argo manual](https://argoproj.github.io/argo-workflows/examples/#artifacts) for a description of artifacts.

Below is a list of the artifacts that have we have tested with Argo:
* [Google Cloud Storage buckets](./gcs-bucket.md)

## Outputs Artifacts.
By default, artifacts output a `tar.tgz` file. Howerver, other output formats are available.
Check the [Argo Doc](https://argoproj.github.io/argo-workflows/examples/#artifacts) for more details.
