# Use of Google Cloud Storage (GCS) buckets in Argo

In this section, we consider that one wants to connect to a bucket from an Argo workflow
running on the Google Kubernetes Engine (GKE). In this case we will use the 
[Workload identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity)
to connect to the Google APIs. This is the recommended way 
to access Google Cloud services from applications running within GKE.

## Enable Workload Identity on your cluster

### GKE Dashboard
The Workload Identity can be enabled via the GKE dashboard when creating the cluster. 
To enable it, you need to go to the Security features of the cluster on the create panel, and select the `Enable Workload Identity`.
The name of the Workload Pool will be given when you select `Enable Workload Identity`.
You can then create the cluster with your own parameters set.

### Terraform
If you create your cluster using Terraform, you can enable the Workload Identity 
adding these lines to you cluster configuration file
```tf
workload_identity_config {
  identity_namespace = "${var.project_id}.svc.id.goog"
}
 ```
**ASK JEROME IF THAT IS ENOUGH**

### Enable existing clusters
If you have already created the clusters, you can enable Workload Identity on them using
[this guide](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity#enable_on_cluster)

## Authenticating to Google Cloud
**NOTE: The actions below have to be made on the target cluster, i.e., that on which the workflow will run!!**

Here is a description of how to authenticate an application to Google Cloud using Workload Identity. Check the [corresponding section](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity#authenticating\_to) in the documentation for more details.

Here is a description of the description of the variables needed to authenticate the app:
* GSA_NAME = `sbo-bucket`: The google service account used. It is set directly in the terraform deployment.
* PROJECT_ID = `irt-sb`: That is the name of the project that is needed for the name of the workload identity cluster.
* KSA_NAME = `argo-workflow`: The name of the service account that will run the workflow. That service account is set when installing Argo for Admiralty
* K8S_NAMESPACE = `default`: Namespace where the workflow is running on on the target cluster

1. Create Google Service Account.  You can manually create the Google service account (GSA_NAME).  In our case, this is done with the terraform deployment.
```
gcloud iam service-accounts create GSA_NAME
```

2. Allow the Kubernetes service account to impersonate the Google service account by creating an [IAM policy binding](https://cloud.google.com/sdk/gcloud/reference/iam/service-accounts/add-iam-policy-binding) between the two. This binding allows the Kubernetes Service account to act as the Google service account.

```bash
gcloud iam service-accounts add-iam-policy-binding --role roles/iam.workloadIdentityUser --member "serviceAccount:irt-sb.svc.id.goog[default/argo-workflow]" sbo-bucket@irt-sb.iam.gserviceaccount.com
```

3. Add the `iam.gke.io/gcp-service-account=GSA_NAME@PROJECT_ID` annotation to the Kubernetes service account, using the email address of the Google service account
```bash
kubectl annotate serviceaccount --namespace default argo-workflow iam.gke.io/gcp-service-account=sbo-bucket@irt-sb.iam.gserviceaccount.com
```

4. Verify the service accounts are configured correctly by creating a Pod with the Kubernetes service account that runs the OS-specific container image, then connect to it with an interactive session.

```bash
kubectl run -it --image google/cloud-sdk:slim --serviceaccount argo-workflow --namespace default workload-identity-test
```

## Run Argo Workflow
Now that the bucket is ready and the Workload Identity has been enabled, you can run Argo worklows
that will use the buckets. 
Check the folder [artifact_gcs_bucket](../argo-workflows/artifact_gcs_bucket) for workflows set with buckets on GCS. 
You can run them as you would with any [Argo workflow with Admiralty](../README.md#launch-workflow). 
**Note** that you need to set your own bucket in the scripts. 
