# Admiralty on GCP

Here is are some small specification in order to use Admiralty on GCP.

The main thing to know when running Admiralty on GCP, is that Kubernetes API server on GCP
listens on port 443. By default, Kubernetes API server listens on port 6443.

[Use Google Cloud Storage (GCS) bucket with Admiralty](./buckets.md)

