# Benchmark of data storage solutions identified to work with Admiralty and Argo
## Persistent volumes
This solution does not seem efficient to us. Dealing with Persistent Volume Claim in Admiralty does not seem to be a good solution.
https://cloud.google.com/kubernetes-engine/docs/concepts/persistent-volumes

## Buckets
### Set workload identity
[Workload identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity) is the recommended way to access Google Cloud services from applications running within GKE. 
Here we supposed that Workload identity is already enabled. If it is not, you can refere to the [Enabling Workload identity on a cluster](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity#enable\_on\_cluster) section of the documentation.

When you [enable Workload Identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity#enable_on_cluster) on your GKE cluster, the cluster's workload identity pool will be set to `PROJECT_ID.svc.id.goog`. This lets IAM authenticate Kubernetes service accounts as the following member name:
```
serviceAccount:PROJECT_ID.svc.id.goog[K8S_NAMESPACE/KSA_NAME]
```
In this member name:

-   `PROJECT_ID.svc.id.goog` is the workload identity pool set on the cluster.
-   `KSA_NAME` is the name of the Kubernetes service account making the request.
-   `K8S_NAMESPACE` is the Kubernetes namespace where the Kubernetes service account is defined.

There is only one fixed workload identity pool per Google Cloud project, `PROJECT_ID.svc.id.goog`, and it is automatically created for you.

### Authenticating to Google Cloud
**NOTE: The actions below have to be made on the target cluster, i.e., that on which the workflow will run!!**

Here is a description of how to authenticate an application to Google Cloud using Workload Identity. Check the [corresponding section](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity#authenticating\_to) in the documentation for more details.

Here is a description of the description of the variables needed to authenticate the app:
* GSA_NAME = `sbo-bucket`: The google service account used. It is set directly in the terraform deployment. 
* PROJECT_ID = `irt-sb`: That is the name of the project that is needed for the name of the workload identity cluster. 
* KSA_NAME = `argo-workflow`: The name of the service account that will run the workflow. That service account is set when installing Argo for Admiralty
* K8S_NAMESPACE = `default`: Namespace where the workflow is running on on the target cluster

1. Create Google Service Account
	You can manually create the Google service account (GSA_NAME).
	In our case, this is done with the terraform deployment.
```bash
gcloud iam service-accounts create GSA_NAME
```


2. Allow the Kubernetes service account to impersonate the Google service account by creating an [IAM policy binding](https://cloud.google.com/sdk/gcloud/reference/iam/service-accounts/add-iam-policy-binding) between the two. This binding allows the Kubernetes Service account to act as the Google service account.

```bash
gcloud iam service-accounts add-iam-policy-binding --role roles/iam.workloadIdentityUser --member "serviceAccount:irt-sb.svc.id.goog[default/argo-workflow]" sbo-bucket@irt-sb.iam.gserviceaccount.com
```

3. Add the `iam.gke.io/gcp-service-account=GSA_NAME@PROJECT_ID` annotation to the Kubernetes service account, using the email address of the Google service account
```bash
kubectl annotate serviceaccount --namespace default argo-workflow iam.gke.io/gcp-service-account=sbo-bucket@irt-sb.iam.gserviceaccount.com
```

4. Verify the service accounts are configured correctly by creating a Pod with the Kubernetes service account that runs the OS-specific container image, then connect to it with an interactive session.

```bash
kubectl run -it --image google/cloud-sdk:slim --serviceaccount argo-workflow --namespace default workload-identity-test 
```